﻿-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Počítač: 127.0.0.1
-- Vytvořeno: Stř 16. pro 2015, 13:52
-- Verze serveru: 5.6.26
-- Verze PHP: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `webconf`
--
CREATE DATABASE IF NOT EXISTS `webconf`;

-- --------------------------------------------------------

--
-- Struktura tabulky `article`
--

CREATE TABLE IF NOT EXISTS `webconf`.`article` (
  `id` int(11) NOT NULL,
  `title` varchar(40) NOT NULL,
  `autors` varchar(40) NOT NULL,
  `content` text NOT NULL,
  `id_post_author` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `accepted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `reviews`
--

CREATE TABLE IF NOT EXISTS `webconf`.`reviews` (
  `id_review` int(11) NOT NULL,
  `id_article` int(11) NOT NULL,
  `id_reviewer` int(11) NOT NULL,
  `idea` int(2) NOT NULL,
  `theme` int(2) NOT NULL,
  `note` varchar(500) NOT NULL,
  `lock_edit` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `role`
--

CREATE TABLE IF NOT EXISTS `webconf`.`role` (
  `id_role` int(11) NOT NULL,
  `role_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struktura tabulky `user`
--

CREATE TABLE IF NOT EXISTS `webconf`.`user` (
  `id_user` int(11) NOT NULL,
  `nick` varchar(25) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `article`
--
ALTER TABLE `webconf`.`article`
  ADD PRIMARY KEY (`id`);

--
-- Klíče pro tabulku `reviews`
--
ALTER TABLE `webconf`.`reviews`
  ADD PRIMARY KEY (`id_review`);

--
-- Klíče pro tabulku `role`
--
ALTER TABLE `webconf`.`role`
  ADD PRIMARY KEY (`id_role`);

--
-- Klíče pro tabulku `user`
--
ALTER TABLE `webconf`.`user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `article`
--
ALTER TABLE `webconf`.`article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `reviews`
--
ALTER TABLE `webconf`.`reviews`
  MODIFY `id_review` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `role`
--
ALTER TABLE `webconf`.`role`
  MODIFY `id_role` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pro tabulku `user`
--
ALTER TABLE `webconf`.`user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT;

--
-- Vypisuji data pro tabulku `user`
--
INSERT INTO `webconf`.`role` (`id_role`, `role_name`) VALUES
(1, 'admin');
INSERT INTO `webconf`.`role` (`id_role`, `role_name`) VALUES
(2, 'autor');
INSERT INTO `webconf`.`role` (`id_role`, `role_name`) VALUES
(3, 'recenzent');
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
