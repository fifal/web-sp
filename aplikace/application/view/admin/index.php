<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Administrace</h4>
            </div>
            <div class="panel-body">
                <nav class="navbar navbar-default">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo URL . 'admin/reviews'; ?>">Hodnocení článků</a></li>
                        <li><a href="<?php echo URL . 'admin/users'; ?>">Správce uživatelů</a></li>
                        <li><a href="<?php echo URL . 'admin/assign'; ?>">Přiřazení článků</a></li>
                        <li><a href="<?php echo URL . 'admin/posts'; ?>">Všechny články</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
