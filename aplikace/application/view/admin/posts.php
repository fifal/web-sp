<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Všechny články</h4>
            </div>
            <div class="panel-body">
                <?php
                if($posts == null){
                    echo 'Žádné články k zobrazení.';
                }
                foreach ($posts as $post) {
                    if (strlen($post->content) > 666) {
                        $dots = "...";
                    } else {
                        $dots = "";
                    }
                    echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '<p style="float:right;">'.$post->date.'</p>
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                }
                ?>
            </div>
        </div>
    </div>
</div>