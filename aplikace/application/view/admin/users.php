<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Správce uživatelů</h4>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Nick</th>
                        <th>E-mail</th>
                        <th>Role</th>
                        <th>Akce</th>
                    </tr>
                    </thead>
                    <?php
                    foreach ($users as $user) {
                        echo '<tr><td>' . $user->nick . '</td><td>' . $user->email . '</td><td>' . $user->role_name . '</td>';
                        echo '<td><input type="button" class="btn btn-xs btn-primary" value="Spravovat" onClick="location.href=\'' . URL . 'admin/manage_user/' . $user->id_user . '\'">';
                        if ($user->role == 1) {
                            echo ' <input type="button" class="btn btn-xs btn-default" value="Smazat" onClick="location.href=\'' . URL . 'admin/delete_user/' . $user->id_user . '\'"disabled></td></tr>';
                        } else {
                            echo ' <input type="button" class="btn btn-xs btn-danger" value="Smazat" onClick="location.href=\'' . URL . 'admin/delete_user/' . $user->id_user . '\'"></td></tr>';
                        }
                    }
                    ?>
                </table>
            </div>
        </div>
    </div>
</div>