<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Editace uživatele</h4>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-2">
                        <img src="<?php echo URL . 'img/user.png' ?>" class="img-responsive" height="150 px"
                             width="150 px">
                    </div>
                    <div class="col-sm-4">
                        <h2><?php echo $nick; ?></h2>
                        <h4>e-mail: <?php echo $email; ?></h4>
                        <h4>role: <?php echo $role; ?></h4>
                    </div>
                    <div class="col-sm-6">
                        <form class="form" method="post"
                              action="<?php echo URL . 'admin/commit_edit_user/' . $id_user; ?>">
                            <br>
                            <input type="text" class="form-control" name="nick" value="<?php echo $nick; ?>" required>
                            <input type="email" class="form-control" name="email" value="<?php echo $email; ?>"
                                   required>
                            <select class="form-control" name="role"
                                    required <?php if ($nick == $_SESSION['nick']) echo 'disabled'; ?>>
                                <option <?php if ($role == "admin") echo 'selected'; ?>>admin</option>
                                <option <?php if ($role == "autor") echo 'selected'; ?>>autor</option>
                                <option <?php if ($role == "recenzent") echo 'selected'; ?>>recenzent</option>
                            </select>
                            <?php if ($nick == $_SESSION['nick']) echo '<input type="hidden" name="role" value=' . $role . '>'; ?>
                            <input type="submit" class="btn btn-success form-control" value="Upravit">
                        </form>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-sm-12">
                        <h4>Uživatel</h4>

                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#userPosts">Články uživatele</a></li>
                                <li><a data-toggle="tab" href="#assignedPosts">Přiřazené články</a></li>
                            </ul>

                            <div class="tab-content">
                                <div id="userPosts" class="tab-pane active">
                                    <?php
                                    foreach ($user_posts as $post) {
                                        if (strlen($post->content) > 666) {
                                            $dots = "...";
                                        } else {
                                            $dots = "";
                                        }
                                        echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                                    }
                                    ?>
                                </div>
                                <div id="assignedPosts" class="tab-pane">
                                    <?php
                                    $title = null;
                                    foreach ($user_assigned as $post) {
                                        if (strlen($post->content) > 666) {
                                            $dots = "...";
                                        } else {
                                            $dots = "";
                                        }
                                        if ($title != $post->title) {
                                            echo '
                                <div class="panel panel-default">
                                    <div class="panel-heading"><b><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></b>,
                                    autor: ' . $post->autors . '
                                    </div>
                                    <div class="panel-body">
                                        ' . substr($post->content, 0, 666) . '<br>' . $dots . '
                                    </div>
                                </div>';
                                            $title = $post->title;
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>