<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Přiřazení článků</h4>
            </div>
            <div class="panel-body">
                <?php
                if ($posts == null) {
                    echo 'Nebyly nalezeny žádné články.';
                } else {
                    echo '<table class="table table-bordered table-striped">
                    <thead>
                    <th>Název článku</th>
                    <th>Autor</th>
                    <th>Recenze</th>
                    <th>Akce</th>
                    </thead>';
                }
                $i = 0;
                $id = 0;
                foreach ($posts as $post) {
                    $selectString = null;
                    $reviews = $this->adminModel->getReviews($post->id);
                    foreach ($reviewers as $rev) {
                        if ($this->adminModel->canReview($rev->id_user, $post->id)) {
                            $selectString .= "<option>" . $rev->nick . "</option>";
                        }
                    }
                    switch (count($reviews)) {
                        case 0:
                            echo '<tr><form method="post" class="form" action="' . URL . 'admin/assign_post/' . $id . '"><input type="hidden" name="id_post" value="' . $post->id . '"><td rowspan="3" style="vertical-align: middle;"><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></td><td rowspan="3" style="vertical-align: middle;">' . $post->autors . '</td><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary"> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary"> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary"> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></form></tr>';
                            $i++;
                            break;
                        case 1:
                            echo '<tr><form method="post" class="form" action="' . URL . 'admin/assign_post/' . $id . '"><input type="hidden" name="id_post" value="' . $post->id . '"><td rowspan="3" style="vertical-align: middle;"><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></td><td rowspan="3" style="vertical-align: middle;">' . $post->autors . '</td><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[0]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"> <input type="hidden" name="username' . $i . '" value="' . $reviews[0]->nick . '"></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary">  <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary">  <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></form></tr>';
                            $i++;
                            break;
                        case 2:
                            echo '<tr><form method="post" class="form" action="' . URL . 'admin/assign_post/' . $id . '"><input type="hidden" name="id_post" value="' . $post->id . '"><td rowspan="3" style="vertical-align: middle;"><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></td><td rowspan="3" style="vertical-align: middle;">' . $post->autors . '</td><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[0]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"> <input type="hidden" name="username' . $i . '" value="' . $reviews[0]->nick . '"></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[1]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"> <input type="hidden" name="username' . $i . '" value="' . $reviews[1]->nick . '"></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '">' . $selectString . '</select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary"> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger" disabled></td></form></tr>';
                            $i++;
                            break;
                        case 3:
                            echo '<tr><form method="post" class="form" action="' . URL . 'admin/assign_post/' . $id . '"><input type="hidden" name="id_post" value="' . $post->id . '"><td rowspan="3" style="vertical-align: middle;"><a href="' . URL . 'post/show_post/' . $post->id . '">' . $post->title . '</a></td><td rowspan="3" style="vertical-align: middle;">' . $post->autors . '</td><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[0]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled>  <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"> <input type="hidden" name="username' . $i . '" value="' . $reviews[0]->nick . '"></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[1]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"> <input type="hidden" name="username' . $i . '" value="' . $reviews[1]->nick . '"></td></tr>';
                            $i++;
                            echo '<tr><td><select class="form-control" name="select' . $i . '" disabled><option>' . $reviews[2]->nick . '</option></select></td><td><input type="submit" name="submit' . $i . '" value="Přiřadit k hodnocení" class="btn btn-sm btn-primary" disabled> <input type="submit" name="delete' . $i . '" value="Odstranit hodnocení" class="btn btn-sm btn-danger"><input type="hidden" name="username' . $i . '" value="' . $reviews[2]->nick . '"></td></form></tr>';
                            $i++;
                            break;
                    }
                    $id++;
                }
                ?>
                </table>
            </div>
        </div>
    </div>
</div>