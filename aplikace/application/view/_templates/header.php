<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo URL . 'css/bootstrap.min.css';?>">
    <link rel="stylesheet" href="<?php echo URL . 'css/fileinput.min.css';?>">
    <link rel="stylesheet" href="<?php echo URL . 'css/style.css';?>">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="<?php echo URL . 'js/fileinput.js';?>"></script>
    <script src="<?php echo URL . 'js/bootstrap.min.js';?>"></script>
    <title><?php echo $title;?></title>
</head>

<body>
    <div class="container">
        <div class="jumbotron">
            <a href="<?php echo URL;?>"><h1>WEB-CONF</h1></a>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <nav class="navbar navbar-default">
                    <ul class="nav navbar-nav">
                        <li><a href="<?php echo URL;?>">Domů</a></li>
                        <li><a href="<?php echo URL . 'posts';?>">Příspěvky</a></li>
                        <?php
                        @session_start();
                        if(isset($_SESSION['id_role'])&& $_SESSION['id_role']!=3){
                            echo '<li><a href="'.URL.'post">Přidat příspěvek</a></li>';
                        }
                        else{
                            echo '<li><a href="'.URL.'review">Hodnotit</a></li>';
                        }
                        ?>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <?php
                            if(isset($_SESSION['id_role'])&& $_SESSION['id_role']==1){
                                echo '<li><a href="'.URL.'admin" style="color:#b612ed;">Administrace</a></li>';
                            }
                            if(isset($_SESSION['nick']) && isset($_SESSION['role'])){
                                echo '<li><a href="'.URL.'profile"><b>'.$_SESSION['nick'].'</b>, '.$_SESSION['role'].'</span></a></li>';
                                echo '<li><a href="'.URL.'logout"'.'><span class="glyphicon glyphicon-log-out"></span> Odhlášení</a></li>';
                            }
                            else{
                                echo '<li><a href="'.URL.'register"'.'><span class="glyphicon glyphicon-user"></span> Registrace</a></li>';
                                echo '<li><a href="'.URL.'login"'.'><span class="glyphicon glyphicon-log-in"></span> Přihlášení</a></li>';
                            }
                        ?>
                    </ul>
                </nav>
            </div>
        </div>