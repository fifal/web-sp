<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění!</strong> <?php echo $msg; ?>
        </div>
    </div>
</div>
<?php
if(isset($_SESSION)){
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><h4>Přidat příspěvek</h4></div>
            <div class="panel-body">
               <form class="form" role="form" action="'. URL . 'post/submit" method="post" id="form">
                   <div class="form-group">
                       <label for="name">Název příspěvku:</label>
                       <input type="text" class="form-control" name="title" required>
                   </div>
                   <div class="form-group">
                       <label for="abstract">Abstrakt:</label>
                       <textarea class="form-control textarea" form="form" name="abstract" rows="15" required></textarea>
                   </div>
                   <div class="form-group">
                       <label for="file">PDF Soubor:</label>
                       <input type="file" class="file" name="file" required>
                   </div>
                   <input type="submit" class="form-control btn btn-success">
               </form>
            </div>
        </div>
    </div>
</div>';
}
?>