<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-warning">
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><b><?php echo $post_title; ?></b>, autor: <?php echo $autors; ?>
                <?php
                if ($canEdit) {
                    echo '<div class="btn-group pull-right">
                    <a href="' . URL . 'post/edit_post/' . $postID . '">
                        <button type="button" class="btn btn-xs btn-primary">
                            <span class="glyphicon glyphicon-edit"></span> Editovat
                        </button>
                    </a>
                    <a href="' . URL . 'post/delete_post/' . $postID . '">
                        <button type="button" class="btn btn-xs btn-danger">
                            <span class="glyphicon glyphicon-remove"></span> Smazat
                        </button>
                    </a>
                    </div>';
                }
                ?>

            </div>
            <div class="panel-body">
                <?php
                echo $content;

                $filesize = filesize(APP . 'uploads/' . $postID . '.pdf');
                $filesize = $filesize / 1024;
                $filesize = round($filesize, 0);
                if ($filesize > 1024) {
                    $filesize = $filesize . ' MB';
                } else {
                    $filesize = $filesize . ' KB';
                }
                ?>
                <hr>
                <a href="<?php echo URL . 'download/get_file/' . $postID; ?>">Stáhnout PDF Soubor</a>
                (<?php echo $filesize; ?>) <p style="float:right;"><?php echo $date;?></p>
            </div>
        </div>
    </div>
</div>