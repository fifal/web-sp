<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění!</strong> <?php echo $msg; ?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Home</h4>
            </div>
            <div class="panel-body">
                <p>Vítejte na webových stránkách WEB-CONF.</p>
            </div>
        </div>
    </div>
</div>
