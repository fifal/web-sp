<?php if (isset($msg)) {
    echo '<div class="row">
    <div class="col-sm-12">
        <div class="alert alert-info">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Upozornění! </strong>' . $msg . '
        </div>
    </div>
</div>';
} ?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-heading"><b><?php echo $post_title; ?></b>, autoři: <?php echo $autors; ?></div>
            <div class="panel-body">
                <?php
                echo $content;

                $filesize = filesize(APP . 'uploads/' . $postID . '.pdf');
                $filesize = $filesize / 1024;
                $filesize = round($filesize, 0);
                if ($filesize > 1024) {
                    $filesize = $filesize . ' MB';
                } else {
                    $filesize = $filesize . ' KB';
                }
                ?>
                <hr>
                <a href="<?php echo URL . 'download/get_file/' . $postID; ?>">Stáhnout PDF Soubor</a>
                (<?php echo $filesize; ?>)
                <hr>
                <div class="panel panel-default">
                    <div class="panel-heading"><h4>Moje hodnocení</h4></div>
                    <div class="panel-body">
                        <form class="form" method="post" action="<?php echo URL . 'review/submit/' ?>">
                            <div class="form-group">
                                <label for="idea">Originalita </label>(1 - nejlepší, 5 - nejhorší):
                                <select class="form-control" name="idea" required <?php if($lock_edit==1) echo 'disabled';?>>
                                    <option <?php if($idea == 1) echo 'selected';?>>1</option>
                                    <option <?php if($idea == 2) echo 'selected';?>>2</option>
                                    <option <?php if($idea == 3) echo 'selected';?>>3</option>
                                    <option <?php if($idea == 4) echo 'selected';?>>4</option>
                                    <option <?php if($idea == 5) echo 'selected';?>>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="theme">Téma </label>(1 - nejlepší, 5 - nejhorší):
                                <select class="form-control" name="theme" required <?php if($lock_edit==1) echo 'disabled';?>>
                                    <option <?php if($theme == 1) echo 'selected';?>>1</option>
                                    <option <?php if($theme == 2) echo 'selected';?>>2</option>
                                    <option <?php if($theme == 3) echo 'selected';?>>3</option>
                                    <option <?php if($theme == 4) echo 'selected';?>>4</option>
                                    <option <?php if($theme == 5) echo 'selected';?>>5</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="note">Poznámka:</label>
                                <textarea class="form-control" name="note" rows="8" <?php if($lock_edit==1) echo 'disabled';?>><?php echo $note;?></textarea></div>
                            <input type="hidden" name="id_post" value="<?php echo $postID;?>">
                            <?php if($lock_edit==1) {
                                echo '<input type="submit" class="btn btn-danger form-control" value="Hodnocení již nelze změnit. Příspěvek uzamčen." disabled>';
                            }
                            else{
                                echo '<input type="submit" class="btn btn-success form-control" name="submit" value="Odeslat hodnocení">';
                            }
                            ?>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>