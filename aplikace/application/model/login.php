<?php

/**
 * Class LoginModel
 * Funkce používané pro přihlášení a registraci
 */
class LoginModel{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     * Vrací true pokud je již uživatelské jméno používáno
     * @param $nick nick
     * @return bool
     */
    public function isNickTaken($nick){
        $nick = $this->db->quote($nick);
        $sql = "SELECT * FROM `user` WHERE `nick`=".$nick;
        $query = $this->db->prepare($sql);
        $query->execute();
        if($query->rowCount()>0){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * Přidání uživatele do DB
     * @param $nick nick
     * @param $email email
     * @param $pass heslo
     */
    public function addUser($nick, $email, $pass){
        $nick = $this->db->quote($nick);
        $email = $this->db->quote($email);
        $pass = md5($pass);
        $pass = $this->db->quote($pass);

        $sql = "INSERT INTO `user`(`id_user`, `nick`, `password`, `email`, `role`) VALUES (DEFAULT,".$nick.",".$pass.",".$email.",2)";
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací pokud uživatel a heslo existují v DB
     * @param $nick nick
     * @param $pass heslo
     * @return mixed
     */
    public function login($nick, $pass){
        $nick = $this->db->quote($nick);
        $pass = $this->db->quote($pass);

        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `nick`=".$nick." AND `password`=".$pass;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
}
?>