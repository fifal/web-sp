<?php

/**
 * Class Model
 * Obecné dotazy
 */
class Model
{
    /**
     * @param object $db A PDO database connection
     */
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     * Přidání postu do DB
     * @param $title titulek
     * @param $autors autor
     * @param $abstract obsah
     * @param $id_author idautora
     * @param $date datum
     */
    public function addPost($title, $autors, $abstract, $id_author, $date)
    {
        $title = $this->db->quote($title);
        $autors = $this->db->quote($autors);
        $abstract = $this->db->quote($abstract);
        $id_author = $this->db->quote($id_author);
        $date = $this->db->quote($date);

        $sql = "INSERT INTO `article`(`id`, `title`, `autors`, `content`,`id_post_author`,`date`,`accepted`) VALUES(DEFAULT," . $title . "," . $autors . "," . $abstract . "," . $id_author . ",".$date.",0)";

        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Upravení postu
     * @param $id id postu
     * @param $title titulek
     * @param $abstract obsah
     */
    public function editPost($id, $title, $abstract){
       // $id = $this->db->quote($id);
        $title = $this->db->quote($title);
        $abstract = $this->db->quote($abstract);

        $sql = "UPDATE `article` SET `title`=".$title.",`content`=".$abstract." WHERE `id`= ".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Smaže post
     * @param $id id postu
     */
    public function deletePost($id){
        $id = $this->db->quote($id);
        $sql = "DELETE FROM `article` WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací autora článku
     * @param $post_id id postu
     * @return mixed
     */
    public function getPostAutorId($post_id){
        $post_id = $this->db->quote($post_id);
        $sql = "SELECT * FROM `article` WHERE `id`=".$post_id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací všechny články
     * @return mixed
     */
    public function getAllPosts(){
        $sql = "SELECT * FROM `article`";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací scvhálené články
     * @return mixed
     */
    public function getAcceptedPosts(){
        $sql = "SELECT * FROM `article` WHERE `accepted`=1";
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací true pokud příspěvek existuje
     * @param $id id postu
     * @return bool
     */
    public function postExists($id){
        $sql = "SELECT * FROM `article` WHERE `id`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        if($query->rowCount()>0){
            return true;
        }
        else{
            return false;
        }
    }
}
