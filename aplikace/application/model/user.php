<?php

/**
 * Class UserModel
 * Dotazy používané uživateli
 */
class UserModel{
    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }

    /**
     * Vrací informace o uživateli
     * @param $id id uživatele
     * @return mixed
     */
    public function getUser($id){
        $sql = "SELECT * FROM `user` JOIN `role` ON `user`.`role`=`role`.`id_role` WHERE `id_user`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací všchny posty uživatele
     * @param $id id uživatele
     * @return mixed
     */
    public function getPosts($id){
        $id = $this->db->quote($id);
        $sql = "SELECT * FROM `article` WHERE `id_post_author` =".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Vrací obsah postu podle id
     * @param $id id postu
     * @return mixed
     */
    public function getPost($id){
        $id = $this->db->quote($id);
        $sql = "SELECT * FROM `article` WHERE `id` =".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }

    /**
     * Ohodnocení postu
     * @param $id id článku
     * @param $reviewer id hodnotitele
     * @param $idea nápad
     * @param $theme téma
     * @param $note poznámka
     */
    public function reviewPost($id, $reviewer,$idea,$theme, $note){
        $note = $this->db->quote($note);
        $sql = "UPDATE `reviews` SET `idea`=".$idea .",`theme`=".$theme.",`note`=".$note." WHERE `id_reviewer`=".$reviewer." AND `id_article`=".$id;
        $query = $this->db->prepare($sql);
        $query->execute();
    }

    /**
     * Vrací hodnocení postu
     * @param $id id postu
     * @param $reviewer id hodnotitele
     * @return mixed
     */
    public function getPostReview($id, $reviewer){
        $sql = "SELECT * FROM `reviews` WHERE `id_article`=".$id." AND `id_reviewer`=".$reviewer;
        $query = $this->db->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
}
?>