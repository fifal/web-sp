<?php

/**
 * Class Error
 * Controller pro zobrazení chybové stránky
 */
class Error extends Controller
{
    /**
     * Zobrazení chybové stránky
     */
    public function index()
    {
        $title = 'WEB-CONF Stránka nenalezena';
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/error/index.php';
        require APP . 'view/_templates/footer.php';
    }
}
