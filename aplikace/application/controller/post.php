<?php

/**
 * Class Post
 * Controller pro přidávání příspěvků
 */
class Post extends Controller
{
    public $msg = null;

    /**
     * Hlavní stránka pro přidání příspěvku
     */
    public function index()
    {
        $title = "WEB-CONF Přidat příspěvek";
        session_start();
        if (isset($_SESSION['nick']) && isset($_SESSION['role']) && $_SESSION['id_role'] != 3) {
            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/post/index.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $msg = "Nejprve se musíte přihlásit nebo nemáte oprávnění přispívat.";
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/message.php';
            require APP . 'view/_templates/footer.php';
        }
    }

    /**
     * Odeslání příspěvku do DB
     */
    public function submit()
    {
        session_start();
        if (isset($_POST['title']) && isset($_SESSION['nick'])) {
            $date = date('Y-m-d H:i:s');
            $this->model->addPost($_POST['title'], $_SESSION['nick'], $_POST['abstract'], $_SESSION['id_user'], $date);
            $postID = $this->db->lastInsertId();
            session_abort();
            $this->uploadFile($postID);
        } else {
            $this->error("Nastala chyba při odesílání formuláře");
        }
    }

    /**
     * Potvrzení úpravy stávajicího příspěvku
     * @param $id příspěvku
     */
    public function commit_edit($id)
    {
        if ($_FILES['file']['name'] != null) {
            $this->uploadFile($id);
        }
        session_start();
        $this->model->editPost($id, $_POST['title'], $_POST['abstract']);
        $this->msg = "Příspěvek byl úspěšně editován.";
        session_abort();
        $this->show_post($id);
    }

    /**
     * Zobrazení příspěvku
     * @param $id post
     */
    public function show_post($id)
    {
        session_start();
        if (isset($_SESSION['nick'])) {
            $title = "WEB-CONF Editace";
            $post = $this->userModel->getPost($id);
            if ($post != null) {
                $post_title = $post[0]->title;
                $autors = $post[0]->autors;
                $date = $post[0]->date;
                $content = nl2br($post[0]->content);
                $postID = $id;
                if ($post[0]->accepted == 0) {
                    $msg = "Článek ještě nebyl schválen správcem.";
                }
                $canEdit = false;
                $query = $this->model->getPostAutorId($id);
                if (($query[0]->id_post_author == $_SESSION['id_user']) || ($_SESSION['id_role'] == 1)) {
                    $canEdit = true;;
                } else {
                    $canEdit = false;
                }
                session_abort();
                require APP . 'view/_templates/header.php';
                require APP . 'view/post/show.php';
                require APP . 'view/_templates/footer.php';
            } else {
                $this->home("Článek neexistuje");
            }
        } else {
            $this->home("Pro zobrazení článku musíte být přihlášeni");
        }

    }

    /**
     * Úprava stávajícího příspěvku
     * @param $id_post id postu
     */
    public function edit_post($id_post)
    {
        session_start();
        $query = $this->model->getPostAutorId($id_post);
        if ($query != null) {
            if (($query[0]->id_post_author == $_SESSION['id_user']) || ($_SESSION['id_role'] == 1)) {
                $title = "WEB-CONF Editace";
                $post = $this->userModel->getPost($id_post);
                if ($post != null) {
                    $post_title = $post[0]->title;
                    $autors = $post[0]->autors;
                    $content = $post[0]->content;
                    $postID = $post[0]->id;

                    session_abort();
                    require APP . 'view/_templates/header.php';
                    require APP . 'view/post/edit.php';
                    require APP . 'view/_templates/footer.php';
                } else {
                    session_abort();
                    $this->home("Snažíte se upravit příspěvek, který neexistuje.");
                }

            } else {
                session_abort();
                $this->home("Můžete upravovat pouze svoje příspěvky");
            }
        } else {
            $this->home("Snažíte se upravit příspěvek, který neexistuje.");
        }

    }

    /**
     * Smazání příspěvku
     * @param $id_post id post
     */
    public function delete_post($id_post)
    {
        session_start();
        $query = $this->model->getPostAutorId($id_post);
        if ($query != null) {
            if ($query[0]->id_post_author == $_SESSION['id_user']) {
                $this->model->deletePost($id_post);
                if (file_exists(APP . 'uploads/' . $id_post . '.pdf')) {
                    unlink(APP . 'uploads/' . $id_post . '.pdf');
                }
                session_abort();
                $this->home("Příspěvek byl smazán.");
            } else {
                session_abort();
                $this->home("Můžete mazat pouze svoje příspěvky");
            }
        } else {
            $this->home("Snažíte se smazat příspěvek, který neexistuje.");
        }

    }

    /**
     * Chybová hláška
     * @param $msg zpráva
     */
    private function error($msg)
    {
        $title = "WEB-CONF Přidat příspěvek";
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Přesměrování na hlavní stránku pokud uživatel není přihlášen
     * @param $msg zpráva
     */
    private function home($msg)
    {
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Nahrání souboru na server, pojmenuje soubor podle ID příspěvku
     * @param $post_id id postu
     */
    private function uploadFile($post_id)
    {
        if (isset($_FILES['file'])) {
            $errors = array();
            $file_name = $_FILES['file']['name'];
            // $file_size =$_FILES['image']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            // $file_type =$_FILES['image']['type'];
            $tmp = explode('.', $file_name);
            $file_ext = strtolower(end($tmp));

            $expensions = array("pdf");

            if (in_array($file_ext, $expensions) === false) {
                $errors[] = "extension not allowed, please choose a JPEG or PNG file.";
            }

//            if($file_size > 2097152){
//                $errors[]='File size must be excately 2 MB';
//            }

            if (empty($errors) == true) {
                move_uploaded_file($file_tmp, APP . 'uploads/' . $post_id . '.pdf');
                header("Location: " . URL . 'post/show_post/' . $post_id);
            } else {
                $this->error("Nepodporovaný typ souboru.");
            }
        }
    }

    /**
     * Svhálení postu podle ID
     * @param $id id postu
     */
    public function approve_post($id)
    {
        session_start();
        $exists = $this->model->postExists($id);
        if ($exists != null) {
            if (isset($_SESSION['id_role']) && $_SESSION['id_role'] == 1) {
                $this->adminModel->approvePost($id);
                $this->adminModel->lockPost($id);

                $assignedPosts = $this->adminModel->getAssignedPosts();
                $reviewers = $this->adminModel->getReviewers();
                $title = 'WEB-CONF Hodnocení článků';
                $msg = "Článek byl schválen.";

                session_abort();
                require APP . 'view/_templates/header.php';
                require APP . 'view/admin/reviews.php';
                require APP . 'view/_templates/footer.php';
            }
        } else {
            $this->home("Snažíte se scvhálit příspěvek, který neexistuje.");
        }
    }

    /**
     * Zamítnutí postu podle id
     * @param $id id postu
     */
    public function reject_post($id)
    {
        session_start();
        $exists = $this->model->postExists($id);
        if ($exists != null) {
            if (isset($_SESSION['id_role']) && $_SESSION['id_role'] == 1) {
                $this->adminModel->rejectPost($id);
                $this->adminModel->lockPost($id);

                $assignedPosts = $this->adminModel->getAssignedPosts();
                $reviewers = $this->adminModel->getReviewers();
                $title = 'WEB-CONF Hodnocení článků';
                $msg = "Článek byl zamítnut.";

                session_abort();
                require APP . 'view/_templates/header.php';
                require APP . 'view/admin/reviews.php';
                require APP . 'view/_templates/footer.php';
            }
        }
        else {
            $this->home("Snažíte se zamítnout příspěvek, který neexistuje.");
        }
    }
}

?>