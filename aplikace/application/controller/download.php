<?php

/**
 * Class Download
 * Controller pro stahování soborů
 */
class Download extends Controller
{
    /**
     * Hlavní stránka
     */
    public function index()
    {
        $title = 'WEB-CONF Home';
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/index.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Vyžádání souboru
     * @param $param id souboru
     */
    public function get_file($param)
    {
        session_start();
        if (isset($_SESSION['nick'])) {
            if (file_exists(APP . 'uploads/' .$param .".pdf")) {
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($param) . '.pdf"');
                readfile(APP . 'uploads/' .$param .".pdf");
            }
        }
        session_abort();
    }
}

?>