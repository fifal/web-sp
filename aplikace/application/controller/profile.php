<?php

/**
 * Class Profile
 * Controller pro zobrazení uživatelského profilu
 */
class Profile extends Controller{
    /**
     * Zobrazení úživatelského profilu
     */
    public function index(){
        $title = "WEB-CONF Profil";
        session_start();
        $nick = null;
        $email = null;
        $role = null;

        if(isset($_SESSION['nick'])){
            $profile = $this->userModel->getUser($_SESSION['id_user']);
            $nick = $profile[0]->nick;
            $email = $profile[0]->email;
            $role = $profile[0]->role_name;
            $user_posts = $this->userModel->getPosts($_SESSION['id_user']);
            $user_assigned = $this->adminModel->getUserAssignedPosts($_SESSION['id_user']);

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/profile/index.php';
            require APP . 'view/_templates/footer.php';
        }
        else{
            session_abort();
            $title = "WEB-CONF Home";
            $msg = "Pro zobrazení profilu musíte být přihlášeni.";
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/message.php';
            require APP . 'view/_templates/footer.php';
        }
    }
}
?>