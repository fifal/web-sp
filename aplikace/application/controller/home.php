<?php

/**
 * Class Home
 * Controller pro hlavní stránku
 */
class Home extends Controller
{
    /**
     * Hlavní stránka
     */
    public function index()
    {
        $title = 'WEB-CONF Home';
        // load views
            require APP . 'view/_templates/header.php';
            require APP . 'view/home/index.php';
            require APP . 'view/_templates/footer.php';
    }
}
