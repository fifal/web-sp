<?php

/**
 * Class Login
 * Controller pro přihlašování
 */
class Login extends Controller{
    /**
     * Přihlašovací stránka
     */
    public function index(){
        $title = 'WEB-CONF Přihlášení';
        require APP . 'view/_templates/header.php';
        require APP . 'view/login/index.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Zpracování formuláře a přihlášení
     */
    public function submit(){
        if(isset($_POST)){
            $nick = $_POST['nick'];
            $pass = md5($_POST['pwd']);

            $login = $this->loginModel->login($nick, $pass);
            $array = (array)$login;
            if(count($array)>0){
                session_start();
                $_SESSION['id_user'] = $login[0]->id_user;
                $_SESSION['nick'] = $login[0]->nick;
                $_SESSION['id_role'] = $login[0]->role;
                $_SESSION['role'] = $login[0]->role_name;
                $this->success("Byl jste úspěšně přihlášen!");
            }
            else{
                $this->error("Chybné uživatelské jméno nebo heslo!");
            }
        }
    }

    /**
     * Chybová stránka
     * @param $msg chyba
     */
    public function error($msg){
        $title = 'WEB-CONF Přihlášení';
        require APP . 'view/_templates/header.php';
        require APP . 'view/login/error.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Hlavní stránka
     * @param $msg zpráva
     */
    public function success($msg){
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }
}
?>