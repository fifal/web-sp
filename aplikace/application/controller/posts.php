<?php

/**
 * Class Posts
 * Controller pro zobrazení všech příspěvků
 */
class Posts extends Controller{
    /**
     * Hlavní stránka, zobrazí všechny přijaté posty
     */
    public function index(){
        session_start();
        if(isset($_SESSION['nick'])){
            $title = "WEB-CONF Příspěvky";
            $posts = $this->model->getAcceptedPosts();

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/posts/index.php';
            require APP . 'view/_templates/footer.php';
        }
        else{
            $this->home("Pro zobrazení příspěvků musíte být přihlášeni.");
        }
    }

    /**
     * Přesměrování na hlavní stránku pokud uživatel není přihlášen.
     * @param $msg zpráva
     */
    private function home($msg){
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }
}
?>