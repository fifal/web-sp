<?php

/**
 * Class Admin
 * Controller pro administraci
 */
class Admin extends Controller
{
    public $msg = null;

    /**
     * Hlavní stránka administrace
     */
    public function index()
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $title = 'WEB-CONF Administrace';
            // load views
            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/index.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $this->home("Musíte být přihlášen.");
        }
    }

    /**
     * Smazání uživatele
     * @param $id id uživatele
     */
    public function delete_user($id)
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $query = $this->adminModel->getUser($id);
            if (isset($query[0])) {
                if ($query[0]->id_role == 1) {
                    $this->message_user("Nemůžete smazat admina.");
                } else {
                    $this->adminModel->deleteUser($id);
                    $this->message_user("Uživatel úspěšně smazán");
                }
            } else {
                $this->message_user("Snažíte se smazat neexistujícího uživatele");
            }
        } else {
            $this->home("Musíte být přihlášen.");
        }
    }

    /**
     * Úprava uživatele
     * @param $id id uživatele
     */
    public function manage_user($id)
    {
        @session_start();
        $title = 'WEB-CONF Administrace';
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $query = $this->adminModel->getUser($id);
            if (isset($query[0])) {
                $nick = $query[0]->nick;
                $email = $query[0]->email;
                $role = $query[0]->role_name;
                $id_user = $id;
                $user_posts = $this->userModel->getPosts($id);
                $user_assigned = $this->adminModel->getUserAssignedPosts($id);

                // load views
                require APP . 'view/_templates/header.php';
                require APP . 'view/admin/manage.php';
                require APP . 'view/_templates/footer.php';
            } else {
                $this->message_user("Snažíte se editovat neexistujícího uživatele");
            }
        } else {
            $this->home("Musíte být přihlášen.");
        }

    }

    /**
     * potvrdit úpravu uživatele
     * @param $id id uživatele
     */
    public function commit_edit_user($id)
    {
        if (isset($_POST['nick'])) {
            session_start();
            if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
                $role = null;
                switch ($_POST['role']) {
                    case "admin":
                        $role = 1;
                        break;
                    case "autor":
                        $role = 2;
                        break;
                    case "recenzent":
                        $role = 3;
                        break;

                }

                $this->adminModel->editUser($id, $_POST['nick'], $_POST['email'], $role);
                if ($_SESSION['id_user'] == $id) {
                    $_SESSION['nick'] = $_POST['nick'];
                }
                $this->manage_user($id);
            } else {
                $this->home("Musíte být přihlášen.");
            }
        } else {
            $this->home("Chyba při odesílání formuláře¨.");
        }
    }

    /**
     * Stránka na přiřazení článků
     */
    public function assign()
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $posts = $this->model->getAllPosts();
            $reviewers = $this->adminModel->getReviewers();
            $title = 'WEB-CONF Přiřazení článků';

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/assign.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $this->home("Musíte být přihlášen.");
        }
    }

    /**
     * Stránka s hodnocením
     */
    public function reviews()
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $assignedPosts = $this->adminModel->getAssignedPosts();
            $reviewers = $this->adminModel->getReviewers();
            $title = 'WEB-CONF Hodnocení článků';

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/reviews.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $this->home("Musíte být přihlášen.");
        }
    }

    /**
     * Stránka na správu uživatelů
     */
    public function users()
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            $users = $this->adminModel->getUsers();
            $title = 'WEB-CONF Správce uživatelů';

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/users.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $this->home("Musíte být přihlášen.");
        }
    }

    /**
     * Přiřazení postu k hodnocení
     * @param $idpost id postu
     */
    public function assign_post($idpost)
    {
        session_start();
        if ((isset($_SESSION['id_role'])) && $_SESSION['id_role'] == 1) {
            for ($i = 0; $i < count($_POST); $i++) {
                $id = $i + ($idpost * 3);
                if (isset($_POST['submit' . $id])) {
                    $user = $this->adminModel->getUserByNick($_POST['select' . $id]);
                    $user_id = $user[0]->id_user;
                    $this->adminModel->assignPost($_POST['id_post'], $user_id);
                    session_abort();
                    $this->success_assign("Článek byl úspěšně přiřazen k hodnocení");
                } elseif (isset($_POST['delete' . $id])) {
                    $user = $this->adminModel->getUserByNick($_POST['username' . $id]);
                    $user_id = $user[0]->id_user;
                    $this->adminModel->deleteAssign($_POST['id_post'], $user_id);
                    session_abort();
                    $this->success_assign("Článek byl úspěšně odebrán z hodnocení");
                }
            }
        }
    }

    /**
     * Stránka se všemi články
     */
    public function posts()
    {
        session_start();
        if (isset($_SESSION['id_role']) && $_SESSION['id_role'] == 1) {
            $title = "WEB-CONF Články";
            $posts = $this->model->getAllPosts();

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/posts.php';
            require APP . 'view/_templates/footer.php';
        }
    }

    /**
     * Zobrazení hodnocení
     * @param $id id článku
     * @param $id_user id uživatele
     */
    public function show_review($id, $id_user)
    {
        session_start();
        if (isset($_SESSION['id_role']) && $_SESSION['id_role'] == 1) {
            $title = "WEB-CONF Hodnocení";
            $post = $this->userModel->getPost($id);
            $post_title = $post[0]->title;
            $autors = $post[0]->autors;
            $content = nl2br($post[0]->content);
            $postID = $id;
            $review = $this->userModel->getPostReview($id, $id_user);
            $nick = $this->adminModel->getUser($id_user);
            $nick = $nick[0]->nick;
            $idea = $review[0]->idea;
            $theme = $review[0]->theme;
            $lock_edit = $review[0]->lock_edit;
            $note = nl2br($review[0]->note);

            session_abort();
            require APP . 'view/_templates/header.php';
            require APP . 'view/admin/show.php';
            require APP . 'view/_templates/footer.php';
        } else {
            session_abort();
            $this->home("Pro zobrazení hodnocení musíte být admin.");
        }
    }

    /**
     * Správce uživatelů + zpráva
     * @param $msg
     */
    private function message_user($msg)
    {
        session_abort();
        $users = $this->adminModel->getUsers();
        $title = 'WEB-CONF Správce uživatelů';
        // load views
        require APP . 'view/_templates/header.php';
        require APP . 'view/admin/users.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Přiřazení článku
     * @param $msg
     */
    private function success_assign($msg)
    {
        $title = "WEB-CONF Přiřazení článků";
        $posts = $this->model->getAllPosts();
        $reviewers = $this->adminModel->getReviewers();

        require APP . 'view/_templates/header.php';
        require APP . 'view/admin/assign.php';
        require APP . 'view/_templates/footer.php';
    }

    /**
     * Návrat na domovskou stránku s hláškou msg
     * @param $msg
     */
    private function home($msg)
    {
        session_abort();
        $title = 'WEB-CONF Home';
        require APP . 'view/_templates/header.php';
        require APP . 'view/home/message.php';
        require APP . 'view/_templates/footer.php';
    }
}

?>